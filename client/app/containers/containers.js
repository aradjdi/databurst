import angular from 'angular';

const containersModule = angular
  .module('main.containers', [])
  .name;

export default containersModule;
