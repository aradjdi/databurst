import './sass/style.scss';

require.ensure([], function () {
  const module = require('./bootstrap');
}, 'bootstrap');

window.CONTEXT = 'browser';
// window.CONTEXT = 'cordova';