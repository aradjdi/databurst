import template from './alerts.html';
import controller from './alerts.controller';
import './alerts.scss';

const alertsComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default alertsComponent;
