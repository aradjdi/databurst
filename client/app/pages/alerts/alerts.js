import alertsComponent from './alerts.component';

const alertsModule = angular
  .module('main.pages.alerts', [])
  .component('alerts', alertsComponent);

export default alertsModule;