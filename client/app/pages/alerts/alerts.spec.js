import AlertsModule from './alerts'
import AlertsController from './alerts.controller';
import AlertsComponent from './alerts.component';
import AlertsTemplate from './alerts.html';

describe('Alerts', () => {
  let $rootScope, makeController;

  beforeEach(window.module(AlertsModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new AlertsController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(AlertsTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
      // component/directive specs
      const component = AlertsComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(AlertsTemplate);
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(AlertsController);
      });
  });
});
