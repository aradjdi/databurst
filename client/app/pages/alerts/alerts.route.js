/** @ngInject */
function lazyLoad($q, $ocLazyLoad) {
  const deferred = $q.defer();

  require.ensure([], function () {
    const module = require('./alerts');
    $ocLazyLoad.load({
        name: module.default.name
    });
    deferred.resolve(module);
  }, 'alerts');

  return deferred.promise;
}

/** @ngInject */
function configRoutes($stateProvider) {  
  $stateProvider.state('app.alerts', {
    'url': '/alerts',
    'template': '<alerts></alerts>',
    resolve: {
      lazyLoad
    }
  });
}

export default configRoutes;