/** @ngInject */
const lazyLoad = function($q, $ocLazyLoad) {
  const deferred = $q.defer();

  require.ensure([], function () {
    const module = require('./app');
    $ocLazyLoad.load({
        name: module.default.name
    });
    deferred.resolve(module);
  }, 'app');

  return deferred.promise;
}

/** @ngInject */
const configRoutes = function($stateProvider) {  
  $stateProvider.state('app', {
    'url': '/app',
    'template': '<app></app>',
    resolve: {
      lazyLoad
    }
  });
}

export default configRoutes;