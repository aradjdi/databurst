import angular from 'angular';

import appComponent from './app.component';
import NavigationAnimation from './animations/NavigationAnimation';

const appModule = angular
  .module('main.pages.app', [NavigationAnimation])
  .component('app', appComponent);

export default appModule;
