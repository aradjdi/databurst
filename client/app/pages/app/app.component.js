import template from './app.html';
import controller from './app.controller';
import './app.scss';

const appComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default appComponent;
