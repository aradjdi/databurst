import angular from 'angular';
import ngAnimate from 'angular-animate';
import TweenLite from 'gsap';

const slideInForward = function (element, completeCallBack) {
  TweenLite.fromTo(element, 0.3,
    {'x': '100%', 'opacity': 1, 'z-index': 2},
    {'x': '0%', 'opacity': 1, 'z-index': 2, 'onComplete': completeCallBack}
  );
};

const slideOutForward = function (element, completeCallBack) {
  TweenLite.fromTo(element, 0.3,
    {'x': '0%', 'opacity': 1, 'z-index': 1},
    {'x': '-10%', 'opacity': 0.7, 'z-index': 1, 'onComplete': completeCallBack}
  );
};

const slideInBackward = function (element, completeCallBack) {
  TweenLite.fromTo(element, 0.3,
    {'x': '-10%', 'opacity': 0.7, 'z-index': 1},
    {'x': '0%', 'opacity': 1, 'z-index': 1, 'onComplete': completeCallBack}
  );
};

const slideOutBackward = function (element, completeCallBack) {
  TweenLite.fromTo(element, 0.3,
    {'x': '0%', 'opacity': 1, 'z-index': 2},
    {'x': '100%', 'opacity': 1, 'z-index': 2, 'onComplete': completeCallBack}
  );
};

function animateIn(direction, element, done) {
  if (direction === 'forward') {
    slideInForward(element, done);
  } else if (direction === 'backward') {
    slideInBackward(element, done);
  }
}

function animateOut(direction, element, done) {
  if (direction === 'forward') {
    slideOutForward(element, done);
  } else if (direction === 'backward') {
    slideOutBackward(element, done);
  }
}

/** @ngInject */
function NavigationAnimation($rootScope) {
  let direction = 'forward';
  $rootScope.$on('backward', () => {
    direction = 'backward';
  });
  $rootScope.$on('forward', () => {
    direction = 'forward';
  });

  function enter(element, done) {
    animateIn(direction, element, done);
  }

  function leave(element, done) {
    animateOut(direction, element, done);
  }

  function addClass(element, className, done) {
    if (className === 'active') {
      animateIn(direction, element, done);
    } else {
      done();
    }
  }

  function beforeRemoveClass(element, className, done) {
    if (className === 'active') {
      animateOut(direction, element, done);
    } else {
      done();
    }
  }

  return {
    enter,
    leave,
    addClass,
    beforeRemoveClass
  };
}

/** @ngInject */
function handleStateChange($rootScope, $previousState) {
  $rootScope.$on('$stateChangeSuccess', (event, toState) => {
    const previousRoute = $previousState.get('navigation');
    if (previousRoute && previousRoute.state === toState) {
      $previousState.forget('navigation');
      $rootScope.$emit('backward');
    } else {
      $previousState.memo('navigation');
      $rootScope.$emit('forward');
    }
  });
}

const navigationAnimationModule = angular
  .module('animations.navigationAnimation', [ngAnimate])
  .animation('.animation-navigation', NavigationAnimation)
  .run(handleStateChange)
  .name;

export default navigationAnimationModule;