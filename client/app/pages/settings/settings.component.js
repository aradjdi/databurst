import template from './settings.html';
import controller from './settings.controller';
import './settings.scss';

const settingsComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default settingsComponent;
