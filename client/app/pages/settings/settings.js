import settingsComponent from './settings.component';

const settingsModule = angular
  .module('main.pages.settings', [])
  .component('settings', settingsComponent);

export default settingsModule;