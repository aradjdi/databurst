/** @ngInject */
function lazyLoad($q, $ocLazyLoad) {
  const deferred = $q.defer();

  require.ensure([], function () {
    const module = require('./settings');
    $ocLazyLoad.load({
        name: module.default.name
    });
    deferred.resolve(module);
  }, 'settings');

  return deferred.promise;
}

/** @ngInject */
function configRoutes($stateProvider) {  
  $stateProvider.state('app.settings', {
    'url': '/settings',
    'template': '<settings></settings>',
    resolve: {
      lazyLoad
    }
  });
}

export default configRoutes;