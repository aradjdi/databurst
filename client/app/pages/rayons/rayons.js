import rayonsComponent from './rayons.component';

const rayonsModule = angular
  .module('main.pages.rayons', [])
  .component('rayons', rayonsComponent);

export default rayonsModule;