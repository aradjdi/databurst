import RayonsModule from './rayons'
import RayonsController from './rayons.controller';
import RayonsComponent from './rayons.component';
import RayonsTemplate from './rayons.html';

describe('Rayons', () => {
  let $rootScope, makeController;

  beforeEach(window.module(RayonsModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new RayonsController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(RayonsTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
      // component/directive specs
      const component = RayonsComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(RayonsTemplate);
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(RayonsController);
      });
  });
});
