/** @ngInject */
function lazyLoad($q, $ocLazyLoad) {
  const deferred = $q.defer();

  require.ensure([], function () {
    const module = require('./rayons');
    $ocLazyLoad.load({
        name: module.default.name
    });
    deferred.resolve(module);
  }, 'rayons');

  return deferred.promise;
}

/** @ngInject */
function configRoutes($stateProvider) {  
  $stateProvider.state('app.rayons', {
    'url': '/rayons',
    'template': '<rayons></rayons>',
    resolve: {
      lazyLoad
    }
  });
}

export default configRoutes;