import template from './rayons.html';
import controller from './rayons.controller';
import './rayons.scss';

const rayonsComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default rayonsComponent;
