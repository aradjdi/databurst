/** @ngInject */
function lazyLoad($q, $ocLazyLoad) {
  const deferred = $q.defer();

  require.ensure([], function () {
    const module = require('./dashboard');
    $ocLazyLoad.load({
        name: module.default.name
    });
    deferred.resolve(module);
  }, 'dashboard');

  return deferred.promise;
}

/** @ngInject */
function configRoutes($stateProvider) {  
  $stateProvider.state('app.dashboard', {
    'url': '/dashboard',
    'template': '<dashboard></dashboard>',
    resolve: {
      lazyLoad
    }
  });
}

export default configRoutes;