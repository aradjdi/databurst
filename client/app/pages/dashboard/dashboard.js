import dashboardComponent from './dashboard.component';

const dashboardModule = angular
  .module('main.pages.dashboard', [])
  .component('dashboard', dashboardComponent);

export default dashboardModule;