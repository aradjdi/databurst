import template from './dashboard.html';
import controller from './dashboard.controller';
import './dashboard.scss';

const dashboardComponent = {
  restrict: 'E',
  bindings: {},
  template,
  controller
};

export default dashboardComponent;
