class LoginController {
  /** @ngInject */
  constructor($state, $previousState) {
    $previousState.forget('navigation');
    this.$state = $state;
  }

  login() {
    this.$state.go('app');
  }
}

export default LoginController;
