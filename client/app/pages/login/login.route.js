/** @ngInject */
function lazyLoad($q, $ocLazyLoad) {
  const deferred = $q.defer();

  require.ensure([], function () {
    const module = require('./login');
    $ocLazyLoad.load({
        name: module.default.name
    });
    deferred.resolve(module);
  }, 'login');

  return deferred.promise;
}

/** @ngInject */
function configRoutes($stateProvider) {  
  $stateProvider.state('login', {
    'url': '/login',
    'template': '<login></login>',
    resolve: {
      lazyLoad
    }
  });
}

export default configRoutes;