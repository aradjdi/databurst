import angular from 'angular';

const commonModule = angular
  .module('main.common', [])
  .name;

export default commonModule;
