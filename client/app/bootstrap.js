
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'ui-router-extras';
import ocLazyLoad from 'oclazyload';

import Common from './common/common';
import Containers from './containers/containers';

import loginRoutesConfig from './pages/login/login.route';
import appRoutesConfig from './pages/app/app.route';

import FastClick from 'fastclick';
FastClick.attach(document.body);

angular.module('main', [
    uiRouter,
    'ct.ui.router.extras',
    ocLazyLoad,

    Common,
    Containers
  ])
  .config(($qProvider, $locationProvider) => {
    $qProvider.errorOnUnhandledRejections(false);
  })
  .config(loginRoutesConfig)
  .config(appRoutesConfig)
  .run(($state, $rootScope) => {
    "ngInject";

    $rootScope.$state = $state;
    $state.go('login');
  });

function bootstrapApp() {
  angular.bootstrap(document, ['main']);
}

if (window.CONTEXT === 'cordova') {
  document.addEventListener('deviceready', () => {
    bootstrapApp();
  }, false);
} else {
  bootstrapApp();
}