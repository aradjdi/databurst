const path    = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const CriticalPlugin = require('webpack-plugin-critical').CriticalPlugin;
// const CordovaPlugin = require('webpack-cordova-plugin');

module.exports = {
  devtool: 'source-map',
  entry: {},
  module: {
    noParse: /node_modules\/localforage\/dist\/localforage.js/,
    loaders: [
      {
        'test': /.json$/,
        'loader': 'json'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'ng-annotate!babel'
      },
      {
        'test': /.html$/,
        'loader': 'html'
      },
      {
        'test': /\.(css|scss|sass)$/,
        'loaders': ['style', 'css', 'postcss', 'sass']
      },
      {
        'test': /\.woff(\?\S*)?$/,
        'loader': 'url?limit=10000&mimetype=application/font-woff'
      },
      {
        'test': /\.woff2(\?\S*)?$/,
        'loader': 'url?limit=10000&mimetype=application/font-woff'
      },
      {
        'test': /\.ttf(\?\S*)?$/,
        'loader': 'url?limit=10000&mimetype=application/octet-stream'
      },
      {
        'test': /\.eot(\?\S*)?$/,
        'loader': 'file'
      },
      {
        'test': /\.svg(\?\S*)?$/,
        'loader': 'url?limit=10000&mimetype=image/svg+xml'
      },
      {
        test: /\.png$/,
        loader: 'url?mimetype=image/png'
      },
      {
        test: /\.jpg$/,
        loader: 'url?mimetype=image/jpg'
      }
    ]
  },
  plugins: [
    // Injects bundles in your index.html instead of wiring all manually.
    // It also adds hash to all injected assets so we don't have problems
    // with cache purging during deployment.
    new HtmlWebpackPlugin({
      template: 'client/index.html',
      inject: 'body',
      hash: true
    }),

    // Automatically move all modules defined outside of application directory to vendor bundle.
    // If you are using more complicated project structure, consider to specify common chunks manually.
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      // minChunks: function (module, count) {
      //   return module.resource && module.resource.indexOf(path.resolve(__dirname, 'client')) === -1;
      // }
    }),

    // new CriticalPlugin({
    //   src: 'index.html',
    //   base: path.resolve(__dirname),
    //   inline: true,
    //   minify: true,
    //   dest: 'index.html'
    // })

    // new CordovaPlugin({
    //   config: 'config.xml',  // Location of Cordova' config.xml (will be created if not found)
    //   src: 'index.html',     // Set entry-point of cordova in config.xml
    //   platform: 'ios', // Set `webpack-dev-server` to correct `contentBase` to use Cordova plugins.
    //   version: true,         // Set config.xml' version. (true = use version from package.json)
    // })
  ]
};
